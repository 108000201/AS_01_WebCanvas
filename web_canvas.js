// important objects
var canvas;
var context;

// global property
var is_in_canvas;
var is_shift_pressed;

// drawing property
var painting_status = "pencil";
var rgb_color = 0;
var brush_width = 10;

//color select
var color_bar_pos;
var color_sel_px;
var color_sel_py;
var cur_color_bar;
var color_bar_drag = false;
var color_sel_drag = false;
var color_bar_timestamp;
var color_sel_timestamp;

// about drawing
var last_x;
var last_y;
var shape_timestamp;
var shape_lastpos_x;
var shape_lastpos_y;
var is_drawing;
var is_texting;

// about texting
var font_size;
var font_family;

// about file
var undo_history;
var canvas_size_history;
var history_step;
var max_step;

window.onload = function() {
    initApp();
};

function initApp() {
    canvas = document.getElementById("main_canvas");
    context = canvas.getContext("2d");
    canvas.addEventListener("mousedown", canvas_mouse_down);
    canvas.addEventListener("mousemove", canvas_mouse_move);
    canvas.addEventListener("mouseup", canvas_mouse_up);
    canvas.addEventListener("mouseenter", canvas_mouse_inout);
    canvas.addEventListener("mouseleave", canvas_mouse_inout);
    canvas.style.cursor = "url('resource/pencil_cursor.png') 0 15, default";
    document.getElementById("brush_width_selection").addEventListener("input", brush_width_change);
    document.getElementById("brush_width_selection").value = 10;
    document.getElementById("font_size_selection").addEventListener("change", function() {
        font_size = parseInt(document.getElementById("font_size_selection").value);
    });
    document.getElementById("font_family_selection").addEventListener("change", function() {
        font_family = document.getElementById("font_family_selection").value;
    });
    document.getElementById("color_bar").addEventListener("mousedown", function(e) {
        color_bar_pos = e.offsetY;
        color_bar_drag = true;
        refresh_color_bar();
    })
    document.getElementById("color_bar").addEventListener("mouseup", function(e) {
        color_bar_drag = false;
    })
    document.getElementById("color_bar").addEventListener("mouseleave", function(e) {
        color_bar_drag = false;
    })
    document.getElementById("color_bar").addEventListener("mousemove", function(e) {
        if (color_bar_drag) {
            color_bar_pos = e.offsetY;
            refresh_color_bar();
        }
    })
    document.getElementById("color_selection").addEventListener("mousedown", function(e) {
        color_sel_px = e.offsetX;
        color_sel_py = e.offsetY;
        color_sel_drag = true;
        refresh_color_selection();
    })
    document.getElementById("color_selection").addEventListener("mouseup", function(e) {
        color_sel_drag = false;
    })
    document.getElementById("color_selection").addEventListener("mouseleave", function(e) {
        color_sel_drag = false;
    })
    document.getElementById("color_selection").addEventListener("mousemove", function(e) {
        if (color_sel_drag) {
            color_sel_px = e.offsetX;
            color_sel_py = e.offsetY;
            refresh_color_selection();
        }
    })
    document.body.addEventListener("keydown", function(e) {
        if (e.key == "Shift") {
            is_shift_pressed = true;
        }
    });
    document.body.addEventListener("keyup", function(e) {
        if (e.key == "Shift") {
            is_shift_pressed = false;
        }
    })

    is_drawing = false;
    is_in_canvas = false;
    is_texting = false;
    is_shift_pressed = false;
    font_size = 15;
    font_family = 'Ariel';
    last_x = 0;
    last_y = 0;
    undo_history = new Array();
    canvas_size_history = new Array();
    history_step = -1;
    max_step = -1;
    canvas.width = (document.documentElement.clientWidth - 250) * 0.9;
    canvas.height = document.documentElement.clientHeight - 150;
    document.getElementById("down_tool_bar").height = (canvas.height - 300);
    let icons = document.getElementsByClassName("btn_icon");
    for (let i = 0; i < icons.length; i++) {
        icons[i].height = (((canvas.height - 400) / 6) > 20) ? (canvas.height - 400) / 6 : 20;
    }
    document.getElementById("pencil_btn").style.backgroundColor = "rgba(0, 255, 126, 0.7)";
    color_bar_pos = 0;
    color_sel_px = 200;
    color_sel_py = 200;
    refresh_color_bar();
    add_history();
}

function canvas_mouse_inout(e) {
    // console.log(e);
    if (e.type == "mouseenter") {
        is_in_canvas = true;
    } else if (e.type == "mouseleave") {
        is_in_canvas = false;
    }
}

function canvas_mouse_down(e) {
    // console.log("last_x=" + last_x + ", last_y=" + last_y);
    if (painting_status == "text") {
        let text_input
        let temp_x = e.offsetX;
        let temp_y = e.offsetY;
        if (!is_texting) {
            text_input = document.createElement("input");
            text_input.type = "text";
            text_input.style.position = "fixed";
            text_input.style.left = e.clientX + 'px';
            text_input.style.top = (e.clientY - 10 / 2) + 'px';
            text_input.addEventListener("change", function(e) {
                put_on_text(temp_x, temp_y);
            })
            text_input.id = 'temp_text_input';
            text_input.focus();

            is_texting = true;
            document.body.appendChild(text_input);
        } else {
            if (document.getElementById('temp_text_input').value == "") {
                document.body.removeChild(document.getElementById('temp_text_input'));
                is_texting = false;
            }
        }
    } else {
        last_x = e.offsetX;
        last_y = e.offsetY;
        shape_timestamp = Date.now();
        is_drawing = true;
    }
}

function canvas_mouse_move(e) {
    if (is_drawing) {
        if (painting_status == "pencil") {
            drawing_line(last_x, last_y, e.offsetX, e.offsetY, true);
            last_x = e.offsetX;
            last_y = e.offsetY;
        } else if (painting_status == 'eraser') {
            drawing_line(last_x, last_y, e.offsetX, e.offsetY, false);
            last_x = e.offsetX;
            last_y = e.offsetY;
        } else if (painting_status == "rectangle" || painting_status == "circle" || painting_status == "triangle" || painting_status == "straight") {
            drawing_shape(last_x, last_y, e.offsetX, e.offsetY);
        }
    }
}

function canvas_mouse_up(e) {
    if (is_drawing) {
        is_drawing = false;
        add_history();
    }
}

function drawing_line(beginX, beginY, endX, endY, is_pen) {
    context.beginPath();
    context.strokeStyle = rgb_color;
    context.fillStyle = rgb_color;
    context.globalCompositeOperation = (is_pen) ? "source-over" : "destination-out";

    context.lineCap = "round";
    context.lineWidth = brush_width;

    context.moveTo(beginX, beginY);
    context.lineTo(endX, endY);
    context.stroke();
    context.closePath();
}

function brush_width_change() {
    brush_width = document.getElementById("brush_width_selection").value;
    document.getElementById("brush_width_value_display").innerHTML = brush_width.toString();
}

function paint_tool_clicked(name) {
    // console.log(name);
    document.getElementById(painting_status + "_btn").style.backgroundColor = "rgba(227, 255, 126, 0.7)";
    painting_status = name;
    if (name == "eraser") {
        canvas.style.cursor = "url('resource/eraser_cursor.png') 0 15, crosshair";
    } else if (name == "pencil") {
        canvas.style.cursor = "url('resource/pencil_cursor.png') 0 15, cell";
    } else if (name == "text") {
        canvas.style.cursor = "url('resource/text_cursor.png') 0 15, text";
    } else {
        canvas.style.cursor = "crosshair"
    }
    document.getElementById(name + "_btn").style.backgroundColor = "rgba(0, 255, 126, 0.7)";
}

function put_on_text(pos_x, pos_y) {
    let temp_text_input = document.getElementById('temp_text_input');
    str = temp_text_input.value;
    context.font = font_size + "px " + font_family;

    is_texting = false;
    context.globalCompositeOperation = "source-over";
    context.fillStyle = rgb_color;
    context.fillText(str, pos_x, pos_y);
    document.body.removeChild(document.getElementById('temp_text_input'));
    add_history();
}

function add_history() {
    history_step++;
    max_step = history_step;
    undo_history[history_step] = context.getImageData(0, 0, canvas.width, canvas.height);
    canvas_size_history[history_step] = new Array();
    canvas_size_history[history_step].push(canvas.width);
    canvas_size_history[history_step].push(canvas.height);
    // console.log("$ move step=" + history_step + ", max=" + max_step + ", size=" + undo_history.length + ", w=" + canvas_size_history[history_step][0]);
}

function undo() {
    if (history_step - 1 >= 0) {
        history_step--;
        // console.log("$ undo step=" + history_step + ", max=" + max_step + ", size=" + undo_history.length);
        canvas.width = canvas_size_history[history_step][0];
        canvas.height = canvas_size_history[history_step][1];
        context.globalCompositeOperation = "source-over";
        context.putImageData(undo_history[history_step], 0, 0);
    }
}

function redo() {
    if (history_step + 1 <= max_step) {
        history_step++;
        // console.log("$ redo step=" + history_step + ", max=" + max_step + ", size=" + undo_history.length);
        canvas.width = canvas_size_history[history_step][0];
        canvas.height = canvas_size_history[history_step][1];
        context.globalCompositeOperation = "source-over";
        context.putImageData(undo_history[history_step], 0, 0);
    }
}

function reset() {
    context.globalCompositeOperation = "source-over";
    context.putImageData(undo_history[0], 0, 0);
    max_step = 0;
    history_step = 0;
}

function download() {
    let link = document.createElement('a');
    link.download = 'filename.png';
    link.href = canvas.toDataURL()
    link.click();
}

function upload() {
    let slot = document.createElement('input');
    slot.setAttribute("type", "file");
    slot.setAttribute("accept", "image/*");
    slot.addEventListener("change", function(e) {
        let img = document.createElement('img');
        img.src = URL.createObjectURL(this.files[0]);
        img.id = "temp_img"
        img.onload = function() {
            let w = img.width;
            let h = img.height;
            // console.log("(" + w + "," + h + ")");
            canvas.height = h;
            canvas.width = w;
            context.globalCompositeOperation = "source-over";
            context.drawImage(img, 0, 0);
            add_history();
        }
    });
    slot.click();
}

function drawing_shape(beginX, beginY, endX, endY) {
    if (Date.now() - shape_timestamp > 10) {
        context.globalCompositeOperation = "source-over";
        context.putImageData(undo_history[history_step], 0, 0);
        context.beginPath();
        context.strokeStyle = rgb_color;
        context.fillStyle = rgb_color;
        context.lineWidth = brush_width;
        if (painting_status == "rectangle") {
            if (is_shift_pressed) {
                let a = Math.ceil(Math.sqrt((endX - beginX) * (endX - beginX) + (endY - beginY) * (endY - beginY)) / (Math.SQRT2));
                context.rect(beginX, beginY, a, a);
            } else {
                context.rect(beginX, beginY, endX - beginX, endY - beginY);
            }
        } else if (painting_status == "circle") {
            let centerX = (beginX + endX) / 2;
            let centerY = (beginY + endY) / 2;
            let radius = Math.sqrt((centerX - beginX) * (centerX - beginX) + (centerY - beginY) * (centerY - beginY));
            context.arc(centerX, centerY, radius, 0, 2 * Math.PI);
        } else if (painting_status == "triangle") {
            context.lineCap = "round";
            context.lineJoin = "round";
            context.moveTo((beginX + endX) / 2, beginY);
            if (is_shift_pressed) {
                let a_x = ((beginX + endX) / 2) - endX;
                let a_y = beginY - endY;
                let b_x = Math.ceil((a_x / 2) - (a_y * Math.sqrt(3) / 2));
                let b_y = Math.ceil((a_y / 2) + (a_x * Math.sqrt(3) / 2));
                context.lineTo(endX, endY);
                context.lineTo(endX + b_x, endY + b_y);
                context.lineTo((beginX + endX) / 2, beginY);
            } else {
                context.lineTo(endX, endY);
                context.lineTo(beginX, endY);
                context.lineTo((beginX + endX) / 2, beginY);
            }
        } else if (painting_status == "straight") {
            context.lineCap = "round";
            context.lineJoin = "round";
            context.moveTo(beginX, beginY);
            context.lineTo(endX, endY);
        }
        context.stroke();
        context.closePath();
        shape_timestamp = Date.now();
    }
}

function refresh_color_bar() {
    if (Date.now() - color_bar_timestamp < 10) return;
    let color_bar = document.getElementById("color_bar");
    let cb_ctx = color_bar.getContext("2d");
    let grd = cb_ctx.createLinearGradient(0, 0, 0, 200);
    // color
    grd.addColorStop(0, "rgb(255,0,0)");
    grd.addColorStop(1 / 6, "rgb(255,255,0)");
    grd.addColorStop(2 / 6, "rgb(0,255,0)");
    grd.addColorStop(3 / 6, "rgb(0,255,255)");
    grd.addColorStop(4 / 6, "rgb(0,0,255)");
    grd.addColorStop(5 / 6, "rgb(255,0,255)");
    grd.addColorStop(1, "rgb(255,0,0)");
    cb_ctx.fillStyle = grd;
    cb_ctx.fillRect(0, 0, 20, 200);

    let cur_px = cb_ctx.getImageData(0, color_bar_pos, 1, 1);
    let color = "rgb(" + cur_px.data[0] + "," + cur_px.data[1] + "," + cur_px.data[2] + ")";
    cb_ctx.fillStyle = color;
    cb_ctx.fillRect(0, (color_bar_pos >= 5) ? color_bar_pos - 5 : 0, 20, 10);
    cb_ctx.strokeStyle = "rgb(0,0,0)";
    cb_ctx.lineWidth = 2;
    cb_ctx.strokeRect(0, (color_bar_pos >= 5) ? color_bar_pos - 5 : 0, 20, 10);
    cur_color_bar = color;
    refresh_color_selection();
}

function refresh_color_selection() {
    if (Date.now() - color_sel_timestamp < 10) return;
    let color_sel = document.getElementById("color_selection");
    let cs_ctx = color_sel.getContext("2d");
    let grd = cs_ctx.createLinearGradient(200, 0, 0, 1);
    cs_ctx.globalCompositeOperation = "destination-out";
    cs_ctx.fillRect(0, 0, 200, 200);
    cs_ctx.globalCompositeOperation = "source-over";
    grd.addColorStop(0, cur_color_bar);
    grd.addColorStop(1, "rgb(255,255,255)");
    cs_ctx.fillStyle = grd;
    cs_ctx.fillRect(0, 0, 200, 1);
    for (let i = 0; i < 200; i++) {
        let col_px = cs_ctx.getImageData(i, 0, 1, 1);
        let col_color = "rgb(" + col_px.data[0] + "," + col_px.data[1] + "," + col_px.data[2] + ")";
        let col_grd = cs_ctx.createLinearGradient(0, 0, 0, 200);
        col_grd.addColorStop(0, col_color);
        col_grd.addColorStop(1, "rgb(0,0,0)");
        cs_ctx.fillStyle = col_grd;
        cs_ctx.fillRect(i, 0, 1, 200);
    }

    let cur_px = cs_ctx.getImageData(color_sel_px, color_sel_py, 1, 1);
    rgb_color = "rgb(" + cur_px.data[0] + "," + cur_px.data[1] + "," + cur_px.data[2] + ")";
    cs_ctx.fillStyle = rgb_color;
    cs_ctx.strokeStyle = (cur_px.data[0] + cur_px.data[1] + cur_px.data[2] > 150) ? "rgb(0,0,0)" : "rgb(255,255,255)";
    cs_ctx.moveTo(color_sel_px, color_sel_py);
    cs_ctx.beginPath();
    cs_ctx.arc(color_sel_px, color_sel_py, 5, 0, 2 * Math.PI);
    cs_ctx.stroke();
    cs_ctx.arc(color_sel_px, color_sel_py, 5, 0, 2 * Math.PI);
    cs_ctx.fill();
    cs_ctx.closePath();
}
